const Route = require("express").Router();
const { check } = require("express-validator");
const RegisterController = require("../../controllers/auth/RegisterController");
const LoginController = require("../../controllers/auth/LoginController");

Route.route("/register").post([
    check("name", "El nombre es obligatorio")
        .not()
        .isEmpty()
        .isLength({ min: 5 })
        .withMessage("El nombre debe tener al menos 5 caracteres"),

    check("lastName", "Los apellidos son obligatorios")
        .not()
        .isEmpty()
        .isLength({ min: 5 })
        .withMessage("Los apellidos deben tener al menos 5 caracteres"),

    check("country", "El pais es obligatorio")
        .not()
        .isEmpty(),

    check("email", "El email es obligatorio")
        .not()
        .isEmpty()
        .isEmail()
        .normalizeEmail()
        .withMessage("Email no valido"),

    check("password", "La contraseña es obligatoria")
        .not()
        .isEmpty()
        .isLength({ min: 6 })
        .withMessage("La contraseña debe tener al menos 6 caracteres"),
], RegisterController.Register);

Route.route("/login").post([
    check("email", "El email es requerido")
        .not()
        .isEmpty()
        .isEmail()
        .normalizeEmail()
        .withMessage("Ingrese un email valido"),

    check("password", "La contraseña es requerida")
        .not()
        .isEmpty()
], LoginController.Login);



module.exports = Route;