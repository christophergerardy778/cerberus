const Route = require("express").Router();
const { check } = require("express-validator");
const verifyToken = require("../../middlewares/auth/verifyToken");
const GroupController = require("../../controllers/groups/GroupController");

Route.route("/members/:id").get([
    verifyToken
], GroupController.getMembers);


module.exports = Route;