const app = require("express")();

const register = require("./users/authentication");

const groups = require("./groups/groups");
const groupImages = require("./groups/groupImages");
const publications = require("./publications/publications");
const members = require("./members/members");

app.use(register);


app.use(groups);
app.use("/group/", groupImages);
app.use("/publications/", publications);
app.use(members);

module.exports = app;