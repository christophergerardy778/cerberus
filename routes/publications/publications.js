const multer = require("multer");
const storage = require("../../storage");
const Route = require("express").Router();
const { check } = require("express-validator");
const verifyToken = require("../../middlewares/auth/verifyToken");
const PublicationController = require("../../controllers/publications/PublicationsController");

Route.route("/create").post([
    verifyToken,

    multer({ storage }).single("img"),

    check("description", "Descripcion obligatoria")
        .not()
        .isEmpty(),

], PublicationController.create);


Route.route("/:id").get([
    verifyToken
], PublicationController.getAllPublications);


module.exports = Route;