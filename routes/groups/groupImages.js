const Route = require("express").Router();
const multer = require("multer");
const verifyToken = require("../../middlewares/auth/verifyToken");
const storage = require("../../storage");
const GroupImageController = require("../../controllers/groups/GroupImageController");

Route.route("/image-update/:id").put([
    verifyToken,
    multer({storage}).single("groupImage"),
], GroupImageController.updateImageGroup);

module.exports = Route;
