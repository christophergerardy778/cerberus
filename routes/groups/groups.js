const Route = require("express").Router();
const { check } = require("express-validator");
const verifyToken = require("../../middlewares/auth/verifyToken");
const GroupController = require("../../controllers/groups/GroupController");

Route.route("/create-group").post([
    verifyToken,

    check("name", "Debes darle un nombre a tu grupo")
        .not()
        .isEmpty(),

    check("userId", "El grupo debe tener un creador")
        .not()
        .isEmpty()

], GroupController.create);


Route.route("/my-groups/:id").get([
    verifyToken,
], GroupController.getAllGroupsByUser);


Route.route("/update-group/:id/").put([
    verifyToken,

    check("name", "El nombre del grupo no debe estar vacio")
        .not()
        .isEmpty(),

], GroupController.updateGroup);


Route.route("/add-members").post([
    verifyToken,

    check("groupId", "ID de grupo vacio o invalido")
        .not()
        .isEmpty(),

    check("userId", "Por favor seleccione un usario a agregar")
        .not()
        .isEmpty()
], GroupController.addMember);


Route.route("/group/:id").get([verifyToken], GroupController.getGroup);


Route.route("/group/:groupId/:userId").delete([verifyToken], GroupController.deleteMember);


Route.route("/group/user/:id").get(verifyToken, GroupController.getUserFromGroup);

module.exports = Route;