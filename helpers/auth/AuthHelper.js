const Model = require("../../models/index");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const env = require("../../env");
require("colors");

    async function emailExist(email) {
        const result = await Model.user.findAndCountAll({  where: { email }});
        return result.count > 0;
    }

    async function userExist(id) {
        const result = await Model.user.findAndCountAll({  where: { id }});
        return result.count > 0;
    }

    async function encryptPassword(password) {
        const salt = await bcryptjs.genSaltSync(10);
        return await bcryptjs.hashSync(password, salt);
    }

    async function generateToken(user) {
        delete user.password;
        delete user.createdAt;
        delete user.updatedAt;
        return await jwt.sign(user, env.sign);
    }

    async function passwordMatched(password, hash) {
        return await bcryptjs.compareSync(password, hash);
    }

module.exports = {
    emailExist,
    encryptPassword,
    generateToken,
    passwordMatched,
    userExist
};