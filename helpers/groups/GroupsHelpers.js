const Model = require("../../models/index");
const MailController = require("../../controllers/emails/MailController");

    async function findGroupAndCount(name, UserId) {
        const group = await Model.groups.findAndCountAll({ where: { UserId, name }, raw: true });
        return group.count;
    }

    async function groupExists(id) {
        const group = await Model.groups.findAndCountAll({ where: { id }, raw: true });
        return group.count > 0;
    }

    async function getPublicationById(id) {
        return Model.groupPublications.findOne({
            where: { id },
            include: [
                {
                    model: Model.user
                },
                {
                    model: Model.publicationsImages
                },
                {
                    model: Model.groups
                }
            ]
        });
    }

    async function sendPublication(emails, templatename, locals) {
        await MailController.sendEmailToMembersGroup(emails, templatename, locals);
    }

    async function sendEmail(groupId, publication) {
        const members = await Model.groupMembers.findAndCountAll({
            where: { groupId },
            include: [Model.user]
        });

        const emails = members.rows.map(member => member.dataValues.user.dataValues.email );

        await sendPublication(emails, "index", {
            groupId,
            groupName: publication.group.dataValues.name,
            user: publication.user.dataValues.name,
            description: publication.description
        });
    }


module.exports = {
    findGroupAndCount,
    groupExists,
    getPublicationById,
    sendPublication,
    sendEmail
};