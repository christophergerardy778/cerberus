'use strict';
module.exports = (sequelize, DataTypes) => {
  const publicationsImages = sequelize.define('publicationsImages', {
    url: DataTypes.TEXT,
    public_id: DataTypes.STRING
  }, {
    defaultScope: {
      attributes: { exclude: ["createdAt", "updatedAt"] }
    }
  });
  publicationsImages.associate = function(models) {
    // associations can be defined here
  };
  return publicationsImages;
};