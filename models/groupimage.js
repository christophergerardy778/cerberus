'use strict';
module.exports = (sequelize, DataTypes) => {
  const groupImage = sequelize.define('groupImage', {
    url: DataTypes.TEXT,
    public_id: DataTypes.STRING
  }, {});
  groupImage.associate = function(models) {
    // associations can be defined here
  };
  return groupImage;
};