'use strict';
module.exports = (sequelize, DataTypes) => {
  const groupPublications = sequelize.define('groupPublications', {
    groupId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    imagePublicationId: DataTypes.INTEGER
  }, {});
  groupPublications.associate = function(models) {
    // associations can be defined here
    groupPublications.hasOne(models.user, {
      foreignKey: "id",
      sourceKey: "userId"
    });

    groupPublications.hasOne(models.groups, {
      foreignKey: "id",
      sourceKey: "groupId"
    });

    groupPublications.hasOne(models.publicationsImages, {
      foreignKey: "id",
      sourceKey: "imagePublicationId"
    })
  };
  return groupPublications;
};