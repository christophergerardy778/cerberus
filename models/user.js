'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.TEXT,
    password: DataTypes.TEXT,
    country: DataTypes.STRING,
    avatar: DataTypes.INTEGER,
    cover: DataTypes.INTEGER
  }, {
    defaultScope: {
      attributes: { exclude: ["createdAt", "updatedAt"] }
    }
  });
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};