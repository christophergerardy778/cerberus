'use strict';
module.exports = (sequelize, DataTypes) => {
  const groups = sequelize.define('groups', {
    name: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    imageId: DataTypes.INTEGER
  }, {});
  groups.associate = function(models) {
    groups.hasOne(models.groupImage, {
      foreignKey: "id",
      sourceKey: "imageId"
    }); // RELATION GROUP - IMAGE COVER


    groups.hasMany(models.groupPublications);

    groups.hasOne(models.user, {
      foreignKey: "id",
      sourceKey: "userId"
    });
  };
  return groups;
};