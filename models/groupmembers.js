'use strict';
module.exports = (sequelize, DataTypes) => {
  const groupMembers = sequelize.define('groupMembers', {
    groupId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  groupMembers.associate = function(models) {
    groupMembers.hasOne(models.groups, {
      foreignKey: "id",
      sourceKey: "groupId"
    });

    groupMembers.hasOne(models.user, {
      foreignKey: "id",
      sourceKey: "userId"
    });
  };
  return groupMembers;
};