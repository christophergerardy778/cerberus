const env = require("../../env");
const transport = require("./email.config");
const emailTemplate = require("email-templates");
require("colors");

class MailController {
    static async sendEmailToMembersGroup(members, templatename, locals) {
        try {
            const template = await MailController.makeTemplate(templatename, locals);

            console.log(`--- TEAMPLATE ---: \n ${template}`.bgCyan);

            const emailOptions = {
                from: env.from,
                to: members,
                subject: "Nueva tarea agregada a tu grupo",
                html: template
            };

            await transport.sendMail(emailOptions);

            console.log(" EMAILS ENVIADOS A LOS MIEMBROS DEL GRUPO  ".bgBlue);

        } catch (e) {
            console.log(e.bgRed);
            return e;
        }
    }

    static async makeTemplate(templateName, locals) {
        try {
            const template = new emailTemplate();
            return await template.render(templateName, locals);
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = MailController;