const mailer = require("nodemailer");
const env = require("../../env");

const transport = mailer.createTransport({
    service: env.service,
    auth: {
        user: env.auth.user,
        pass: env.auth.pass
    }
});

module.exports = transport;