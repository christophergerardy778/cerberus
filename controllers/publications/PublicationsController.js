const fs = require("fs-extra");
const Model = require("../../models/index");
const { validationResult } = require("express-validator");
const MakeError = require("../../helpers/response/MakeError");
const MakeResponse = require("../../helpers/response/MakeResponse");
const UploadImageController = require("../uploads/UploadImageController");
const { getPublicationById, sendEmail } = require("../../helpers/groups/GroupsHelpers");

class PublicationsController {
    static async create(req, res) {
        const { errors } = validationResult(req);
        if (errors.length > 0) return res.send(MakeError(errors));
        const { groupId, userId, description } = req.body;

        try {
            if (req.file) {

                const { public_id, secure_url } = await UploadImageController.UploadPhoto(req.file.path);

                const imgPublication = await Model.publicationsImages.create({
                    public_id,
                    url: secure_url
                });

                const publication = await Model.groupPublications.create({
                    groupId,
                    userId,
                    description,
                    imagePublicationId: imgPublication.dataValues.id,
                });

                await fs.unlink(req.file.path);

                const result = await getPublicationById(publication.dataValues.id);

                await sendEmail(groupId, result.dataValues);

            } else {
                const publication = await Model.groupPublications.create({
                   groupId,
                   userId,
                   description
                });

                const result = await getPublicationById(publication.dataValues.id);

                await sendEmail(groupId, result.dataValues);
            }

            return res.send(MakeResponse("Publicacion creada"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


    static async getAllPublications(req, res) {
        try {
            const publications = await Model.groups.findAll({
                where: { id: req.params.id },
                include: [
                    {
                      model: Model.groupImage,
                    },
                    {
                        model: Model.groupPublications,
                        include: Model.user,
                    },
                    {
                        model: Model.groupPublications,
                        include: Model.publicationsImages
                    }
                ],
                order:[
                    [Model.groupPublications, "id", "DESC"]
                ]
            });

            return res.send(MakeResponse(publications));
        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


}

module.exports = PublicationsController;