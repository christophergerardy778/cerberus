const Model = require("../../models/index");
const { validationResult } = require("express-validator");
const MakeError = require("../../helpers/response/MakeError");
const MakeResponse = require("../../helpers/response/MakeResponse");
const { findGroupAndCount, groupExists } = require("../../helpers/groups/GroupsHelpers");
const { userExist } = require("../../helpers/auth/AuthHelper");

class GroupController {
    static async create(req, res) {
        const { errors } = validationResult(req);
        if (errors.length > 0) return res.send(MakeError(errors));
        const { name, userId } = req.body;

        try {
            const groupsExisting = await findGroupAndCount(name, userId);
            if (groupsExisting > 0) return res.send(MakeError("Ya tienes un grupo con este nombre"));

            const group = await Model.groups.create({
                name,
                userId
            });

            await Model.groupMembers.create({
                userId,
                groupId: group.dataValues.id
            });

            return res.send(MakeResponse("Grupo creado correctamente"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


    static async getAllGroupsByUser(req, res) {
        const { errors } = validationResult(req);
        if (errors.length > 0) return res.send(MakeError(errors));

        try {

            const userId = req.params.id;

            const groups = await Model.groupMembers.findAll({
                where: { userId },
                include: [Model.groups]
            });

            return res.send(MakeResponse(groups)); // TODO RELATION WITH IMAGE TABLE

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`))
        }
    }


    static async updateGroup(req, res) {
        const { errors } = validationResult(req);
        if (errors.length > 0) return res.send(MakeError(errors));

        const { name, userId } = req.body;
        const groupId = req.params.id;

        try {

            const groupsExisting = await findGroupAndCount(name, userId);
            if (groupsExisting > 0) return res.send(MakeError("Ya tienes un grupo con este nombre"));

            await Model.groups.update(
                { name },
                { where: { id: groupId } }
            );

            return res.send(MakeResponse(`Grupo actializado`));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


    static async getGroup(req, res) {
        try {
            const GroupId = req.params.id;

            const group = await Model.groups.findOne({
                where: { id: GroupId },
                include: [Model.user]
            });

            return res.send(MakeResponse(group));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


    static async addMember(req, res) {
        const { errors } = validationResult(req);
        if (errors.length > 0) return res.send(MakeError(errors));
        const { groupId, userId } = req.body;

        try {
            const group = await groupExists(groupId);
            const user = await userExist(userId);

            if (!group) return res.send(MakeError("Este grupo no existe"));
            if (!user) return res.send(MakeError("Este Usuario no existe"));

            const userAlreadyMember = await Model.groupMembers.findAndCountAll({
                where: { userId, groupId }
            });

            if (userAlreadyMember.count > 0) {
                return res.send(MakeError("Ya eres miembro de este grupo"));
            }

            await Model.groupMembers.create({
               userId,
               groupId
            });

            return res.send(MakeResponse("Usuario agregado con exito"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }


    static async getMembers(req, res) {
        try {
            const groupId = req.params.id;

            const members = await Model.groupMembers.findAll({
                where: { groupId },
                include: [
                    {
                        model: Model.user,
                    }
                ]
            });

            return res.send(MakeResponse(members));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVE ERROR: ${e}`));
        }
    }


    static async deleteMember(req, res) {
        try {
            const groupId = req.params.groupId;
            const userId = req.params.userId;

            await Model.groupMembers.destroy({
                where: { groupId, userId  }
            });

            return res.send(MakeResponse("Miembro eliminado del grupo"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }

    static async getUserFromGroup(req, res) {
        try {
            const id = req.params.id;

            const user = await Model.user.findOne({
                where: { id }
            });

            return res.send(MakeResponse(user));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }
}

module.exports = GroupController;