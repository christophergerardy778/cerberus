const fs = require("fs-extra");
const Model = require("../../models/index");
const MakeError = require("../../helpers/response/MakeError");
const MakeResponse = require("../../helpers/response/MakeResponse");
const UploadImageController = require("../uploads/UploadImageController");

class GroupImageController {
    static async updateImageGroup(req, res) {
        const groupId = req.params.id;
        const file = req.file.path;

        try {
            const image = await UploadImageController.UploadPhoto(file);

            const { id, imageId } = await Model.groups.findOne({
                raw: true,
                where: { id: groupId },
            });

            if (!imageId) {
                const Image = await Model.groupImage.create({
                    url: image.secure_url,
                    public_id: image.public_id
                });

                await Model.groups.update(
                    { imageId: Image.id },
                    { where: { id } }
                );

            } else {
                const { id, public_id } = await Model.groupImage.findOne({
                    raw: true,
                    where: { id: imageId },
                });

                await UploadImageController.destroyPhoto(public_id);

                await Model.groupImage.update(
                    { url: image.secure_url, public_id: image.public_id },
                    { where: { id: id }}
                );
            }

            await fs.unlink(file);

            return res.send(MakeResponse("Imagen de grupo actualizada"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }
}

module.exports = GroupImageController;