const cloud = require("cloudinary");
const env = require("../../env");

class UploadImageController {
    static async UploadPhoto(file) {

        cloud.config({
            cloud_name: env.CLOUD_NAME,
            api_key: env.CLOUD_API_KEY,
            api_secret: env.CLOUD_API_SECRET
        });

        return await cloud.v2.uploader.upload(file);
    }


    static async destroyPhoto(id) {
        cloud.config({
            cloud_name: env.CLOUD_NAME,
            api_key: env.CLOUD_API_KEY,
            api_secret: env.CLOUD_API_SECRET
        });


        return await cloud.v2.uploader.destroy(id);
    }
}

module.exports = UploadImageController;