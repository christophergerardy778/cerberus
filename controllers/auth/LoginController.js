const Model = require("../../models/index");
const { validationResult } = require("express-validator");
const MakeError = require("../../helpers/response/MakeError");
const MakeResponse = require("../../helpers/response/MakeResponse");
const { emailExist, generateToken, passwordMatched } = require("../../helpers/auth/AuthHelper");

class LoginController {
    static async Login(req, res) {
        const { errors } = validationResult(req);

        if (errors.length > 0) return res.send(MakeError(errors));

        try {
            const { email, password } = req.body;
            const EmailExists = await emailExist(email);

            if (!EmailExists) return res.send(MakeError("Este email no existe"));

            const user = await Model.user.findOne({ where: { email } });
            const match = await passwordMatched(password, user.dataValues.password);

            if (!match) return res.send(MakeError("CONTRASEÑA INCORRECTA"));
            const token = await generateToken(user.dataValues);

            return res.send(MakeResponse(token));

        } catch (e) {
            res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }
}

module.exports = LoginController;