const Model = require("../../models/index");
const { validationResult } = require("express-validator");
const MakeError = require("../../helpers/response/MakeError");
const MakeResponse = require("../../helpers/response/MakeResponse");
const { emailExist, encryptPassword, generateToken } = require("../../helpers/auth/AuthHelper");

class RegisterController {
    static async Register(req, res) {

        const { errors } = validationResult(req);

        if (errors.length > 0) return res.send(MakeError(errors));

        try {
            const { name, lastName, email, password, country } = req.body;

            const emailExists = await emailExist(email);

            if (!emailExists) {
                const result = await Model.user.create({
                    name,
                    email,
                    country,
                    lastName,
                    password: await encryptPassword(password)
                });

                const token = await generateToken(result.dataValues);

                return res.send(MakeResponse(token));
            }

            return res.send(MakeError("EMAIL EN USO"));

        } catch (e) {
            return res.send(MakeError(`INTERNAL SERVER ERROR: ${e}`));
        }
    }
}

module.exports = RegisterController;