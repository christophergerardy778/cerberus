const { diskStorage } = require("multer");
const path = require("path");

const storage = diskStorage({
    destination: path.resolve(__dirname, "tmp/"),

    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

module.exports = storage;