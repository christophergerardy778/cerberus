const jwt = require("jsonwebtoken");
const MakeError = require("../../helpers/response/MakeError");
const env = require("../../env");

module.exports = async (req, res, next) => {
    try {
        const token = req.headers["authorization"];
        await jwt.verify(token, env.sign);
        next();
    } catch (e) {
        return res.send(MakeError(`Autenticacion fallida vuelva a iniciar session`));
    }
};