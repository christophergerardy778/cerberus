const history = require("connect-history-api-fallback");
const router = require("./routes/router");
const express = require("express");
const morgan = require("morgan");
const clear = require("clear");
const https = require("https");
const path = require("path");
const cors = require("cors");
const env = require("./env");
const fs = require("fs");
const app = express();

require("colors");

/*
const cert = fs.readFileSync ("./ssl/christophergerardy_me.crt");
const ca = fs.readFileSync ("./ssl/christophergerardy_me.ca-bundle");
const key = fs.readFileSync ("./ssl/christophergerardy.me.key");
*/

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(history());
app.use("/server/", router);
app.use(express.static(path.resolve(__dirname, 'public/')));

/*
const ssl = {cert, ca, key};
const server = https.createServer(ssl, app);
*/


app.listen(env.port,() => {
    clear();
    console.log(`Server on port: ${env.port}`.bgGreen);
});